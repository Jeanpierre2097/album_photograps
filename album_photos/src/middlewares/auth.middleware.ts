import { Request, Response, NextFunction  } from 'express';

export const isLoggedIn = (req: Request, res: Response, next: NextFunction) => {
  console.log("isLoggedIn middleware")
  req.isAuthenticated() ? next() : res.redirect('/login')
}

// export const isActive = (req: any, res: Response, next: NextFunction) => {
//   console.log("isActive middleware")
//   req.user.active ? next() : res.send('Confirm your account');
// }

// export const authGlobal = (req: any, res: Response, next: NextFunction) => {
//   if(req.isAuthenticated()){
//     req.app.locals.logged = true;
//   } else {
//     req.app.locals.logged = false;
//   }
//   next()
// }