import { Request, Response, NextFunction } from 'express';

export const isLoggedIn = (req: Request, res: Response, next: NextFunction) => {
  req.isAuthenticated() ? next() : res.redirect('/login');
}