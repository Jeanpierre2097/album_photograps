import { Request, Response, NextFunction } from 'express';

export const isLoggedIn = (req: Request, res: Response, next: NextFunction) => {
    if(req.session.passport) {
        next()
    } else{
        res.redirect('/login');
    }   
}


export const checkId = (req: any, res: Response, next: NextFunction) => {
    if(req.session.passport) {
      req.params.id === req.session.passort.user ? next() : res.redirect(`/place/${req.params.id}`);
    } else {
      res.redirect('/login');
    }
}
