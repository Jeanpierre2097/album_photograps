import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm'
import { Place } from '../models/places'


export const getPlaceCreate = (req: any, res: Response) => {

    const { user } = req;
    res.render('place/create', {user});
}

export const postPlaceCreate = async (req: Request, res: Response, next: NextFunction) => {
    const { name, address, latitude, longitude, placeType, idUser } = req.body;
    const newPlace = {
      name, 
      user:idUser, 
      placeType,
      location: {
        address,
        coordinates: [ longitude, latitude ]
      }
    }
    const place = getRepository(Place).create(newPlace);
    await getRepository(Place).save(place);
    console.log('lugar insertado');
    res.redirect('/private-page');
}

export const getPlaceDetail = async(req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  const place = await getRepository(Place).findOne(id);
  res.render('place/placeDetail', place)
}

export const getPlaceUpdate = async (req: Request, res: Response, next: Function) => {
  const { id } = req.params;
  const { user } = req;
  try {
    const place = await getRepository(Place).findOne(id);
    res.render("place/editPlace", {place,user});
  } catch (error) {
    console.log(error);
    next();
  }
}

export const postPlaceUpdate = async (req: Request, res: Response, next: Function) => {
  const { id } = req.params;
  const { name, address, latitude, longitude, placeType, idUser } = req.body;
  console.log('la direccion es: ',address);
  try {
    await getRepository(Place).findOne(id);
    await getRepository(Place).update(id, {
      name, 
      user:idUser, 
      location: {
        address,
        coordinates: [ longitude, latitude ]
      }
    });      
    res.redirect("/private-page");
  } catch (error) {
    console.log(error);
    next();
  }
}

export const PlaceDelete = async (req: Request, res: Response, next: Function) => {
  const { id } = req.params;
  try {
    await getRepository(Place).delete(id);
    res.redirect('/private-page');
  } catch (error) {
    console.log(error);
    next();
  }
}