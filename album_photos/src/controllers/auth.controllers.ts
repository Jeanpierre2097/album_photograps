import { Request, Response, NextFunction } from 'express';
import { User } from '../models/User.entity';
import { Place } from '../models/places'
import { getRepository } from 'typeorm';
import passport from '../config/passport.config';
import bcrypt from 'bcrypt';

export const getSignup = (req: Request, res: Response, next: NextFunction) => {
  res.render('auth/signup')
}

export const postSignup=(req:Request,res:Response)=>{
  req.logout();
  const {name,email,password}=req.body;
if (name === '' && email === '' && password === '') { //si el usuario o contraseña estan vacios, manda mensaje de error
  res.render("auth/signup", { message: "Por favor llene todos los campos" })
}
getRepository(User).findOne({email})
.then(user=>{
    if(!user){
      const salt = bcrypt.genSaltSync(10);
      const hashPassword = bcrypt.hashSync(password, salt);
      const newUser = getRepository(User).create({ name,email, password: hashPassword });
      getRepository(User).save(newUser)
      .then(()=>{
          res.redirect('/');
      })
      .catch(err => console.log(err));
    }else{
      res.render("auth/signup", { message: `El correo ${email} ya ha sido registrado` });
    }
}).
catch(error => console.log(error));
}


export const getLogin = (req: any, res: Response, next: NextFunction) => {
    let message =req.flash("error")[0];
    res.render('auth/login',{message})
  };
//PROFILE

export const profileGet =  (req: any, res: Response, next: NextFunction) => {
  const { user } = req;
  res.render('private-page', user);
}
export const profilePost = async (req: any, res: Response, next: NextFunction) => {
  console.log("Que nos responde cloudinary: ", req.file);
  const { path } = req.file;
  const { id } = req.user; 
  const user = await getRepository(User).update(id, { photoUrl: path });
  req.user = user;
  res.redirect('/private-page')
}

  /**
   * Se requieren dos rutas para la autenticación OAuth. 
   * La primera ruta inicia una transacción OAuth y redirige al usuario al proveedor de servicios. 
   * La segunda ruta es la URL a la que se redirigirá al usuario después de autenticarse con el proveedor.
   */
  export const postLogin = passport.authenticate("local", {
    successRedirect: "/private-page",
    failureRedirect: "/login",
    failureFlash: true
  });

export const logout = (req: any, res: Response, next: NextFunction) => {
  req.session.destroy(() => {
    req.logOut();
    res.clearCookie('graphNodeCookie');
    res.status(200);
    res.redirect('/login');
   })
}

export const privatePageGet = async (req: any, res: Response, next: NextFunction) => {
  const { user } = req;
    const userId = user['id'];
    console.log('El usuario que quiere ver sus fotos es: ',user['email']);
    try {
        const places = await getRepository(Place)
                      .createQueryBuilder("place")
                      .where("user_id = :user_id", { user_id: userId })
                      .orderBy("place.create_at", "ASC").execute();
    
        console.log(places)
      res.render('private-page', {places,user});
      } catch (err) {
        console.log(err)
      }
}

export const getGoogle = (passport.authenticate("google", {
  scope: [
    "https://www.googleapis.com/auth/userinfo.profile",
    "https://www.googleapis.com/auth/userinfo.email"
  ]
})
);
export const getGoogleCB=(passport.authenticate("google", {
  successRedirect: "/private-page",
  failureRedirect: "/login"
})
);

export const getFacebook = (passport.authenticate("facebook", {
scope : ['email'] 
})
);
export const getFacebookCB=(passport.authenticate("facebook", {
successRedirect: "/private-page",
failureRedirect: "/login"
})
);  