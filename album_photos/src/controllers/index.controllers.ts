import { Request, Response, NextFunction } from 'express';
import { Place } from '../models/places'
import { getRepository } from 'typeorm';
import { Photo } from '../models/Photo.entity';

export const getIndex = async (req: any, res: Response, next: NextFunction) => {
    const { user } = req;
    res.render('index', {user});
}
export const getPhotoView = (req: Request, res: Response, next: NextFunction ): void => {
  res.render('photo');
}
export const postCreatePhto = async (req: Request, res: Response, next: NextFunction ): Promise<void> => {
  const { title, description } = req.body;
  const { path, originalname} = req.file;
  const newPhoto = getRepository(Photo).create({ title, description, imgPath: path, imgName: originalname });
  await getRepository(Photo).save(newPhoto);
  res.redirect('/');
}
export const getAllPlaces = async (req: Request, res: Response, next: NextFunction) => {
      try {
          const places = await getRepository(Place)
                        .createQueryBuilder("place")
                        .orderBy("place.create_at", "ASC").execute();
      
          console.log(places)
        res.render('feeds', { places });
        } catch (err) {
          console.log(err)
        }
  }