import { Router} from 'express';
import { isLoggedIn } from '../middlewares/auth.middlewares';
import {getPlaceCreate, postPlaceCreate,getPlaceDetail,PlaceDelete, getPlaceUpdate, postPlaceUpdate} from '../controllers/place.controllers';
const router = Router();

router.get('/place/create',isLoggedIn,getPlaceCreate);
router.post('/place/create',postPlaceCreate);
router.get('/place/:id',getPlaceDetail); //este no requiere inicio de sesion
router.get("/place/:id/delete",isLoggedIn,PlaceDelete);
router.get("/place/:id/edit",isLoggedIn,getPlaceUpdate);
router.post("/place/:id/edit",isLoggedIn,postPlaceUpdate);

export default router;