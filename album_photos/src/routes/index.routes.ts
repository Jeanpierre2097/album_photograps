import { Router, Request, Response } from 'express';
import { isLoggedIn } from '../middlewares/auth.middlewares';
import uploadCloud from '../config/cloudinary.config';
const router = Router();
import {getIndex,getAllPlaces,getPhotoView,postCreatePhto} from '../controllers/index.controllers';

router.get('/', getIndex);
router.get('/feeds', getAllPlaces);
router.get('/photo/add', isLoggedIn,  getPhotoView);
router.post('/photo/add', isLoggedIn, uploadCloud.single('imgPath') , postCreatePhto);
export default router;