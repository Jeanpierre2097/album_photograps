import { Router } from 'express';
import { isLoggedIn } from '../middlewares/auth.middlewares';
import passport from '../config/passport.config';

import { getSignup , logout , getLogin , postLogin , postSignup,
   getGoogle, getGoogleCB, getFacebook, getFacebookCB,  profileGet,
   profilePost, privatePageGet } from '../controllers/auth.controllers';

   import uploadCloud from '../config/cloudinary.config';


const router = Router();

router.get('/signup', getSignup );
router.post('/signup', postSignup);

router.get('/login', getLogin);
router.post('/login', postLogin);

router.get('/logout', logout);

router.get('/private-page', isLoggedIn, privatePageGet,profileGet);

router.post('/private-page', isLoggedIn,  uploadCloud.single('photoUrl'), profilePost)


// Google auth Routes
router.get("/auth/google",getGoogle);
router.get("/auth/google/callback",passport.authenticate("google", {
    successRedirect: "/private-page",
    failureRedirect: "/login"
  }));

router.get('/auth/facebook',getFacebook);
router.get('/auth/facebook/callback',getFacebookCB);

export default router;