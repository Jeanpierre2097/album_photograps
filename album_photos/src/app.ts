#!/usr/bin/env node
require('dotenv').config();

import 'reflect-metadata'
import express from 'express';
import favicon from 'serve-favicon';
import hbs from 'hbs';
import logger from 'morgan';
import path from 'path';
import { createConnection } from 'typeorm';


import ExpressSession from 'express-session';
import { TypeormStore } from 'connect-typeorm/out';
import { Session } from './models/Session.entity';
import passport from './config/passport.config';
import flash from 'connect-flash';

import index from './routes/index.routes';
import routes from './routes/auth.routes';
import place from './routes/place.routes';

async function bootstrap() {
  const app_name = require('../package.json').name;
  const debug = require('debug')(`${app_name}:${path.basename(__filename).split('.')[0]}`);


  const database_usr= process.env.ENV==='development'
                      ?`postgres://${process.env.DB_USER}:${process.env.DB_PASSWORD}@localhost:5432/${process.env.DB}`
                      : process.env.DATABASE_URL;

  //DB connection 

  const connection = await createConnection({
    type: "postgres",
    url: database_usr,
    entities: [path.join(__dirname, './models/*.{ts,js}') ],
    synchronize: true
  });
  // display DB info
  console.log("connected to PostgreSQL! Database options: ", connection.options );

  // setup an express app
  const app = express();

  // Middleware Setup
  app.use(logger('dev'));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));

  // Express View engine setup
  app.use(require('node-sass-middleware')({
    src:  path.join(__dirname, 'public'),
    dest: path.join(__dirname, 'public'),
    sourceMap: true
  }));
        

  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'hbs');
  hbs.registerPartials( path.join(__dirname, 'views/partials' ) )
  app.use(express.static(path.join(__dirname, 'public')));
  app.use(favicon(path.join(__dirname, 'public', 'images', 'favicon.ico')));

  // default value for title local
  app.locals.title = 'Express - Generated with kavak typescript generator';

  app.use(flash());
  // Configure express-session
  app.use(
    ExpressSession({
      resave: false,
      saveUninitialized: false,
      secret: "basic-auth-secret", 
      cookie: { maxAge: 1000 * 60 * 60 * 24 },
      //cookie: { maxAge: 60000 },
      store: new TypeormStore({
        cleanupLimit: 2, //para cada nueva sesion elimina las que han caducado
        limitSubquery: false,
        ttl: 86400  //Tiempo de vida de la sesión (caducidad) en segundos
      }).connect(connection.getRepository(Session))
    })
  )

  // passport
  app.use(passport.initialize());
  app.use(passport.session());


  app.use('/', index);
  app.use('/', routes);
  app.use('/', place);

  return app;
}

module.exports = bootstrap;