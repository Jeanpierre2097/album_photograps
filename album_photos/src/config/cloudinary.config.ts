const cloudinary = require("cloudinary").v2;
import { CloudinaryStorage } from 'multer-storage-cloudinary';
import multer from 'multer';

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_NAME,
  api_key: process.env.CLOUDINARY_KEY,
  api_secret: process.env.CLOUDINARY_SECRET
});

const storage = new CloudinaryStorage({
  cloudinary,
  params: {
    folder: 'proyecto', // foldername in cloudinary
    allowed_formats: ['mp4','ogv','jpg','png','pdf','gif']
    //format: async (req, file) => 'png', // supports promises as well
    //public_id: (req, file) => 'computed-filename-using-request',
  },
});

const uploadCloud = multer({ storage: storage });

export default uploadCloud;

