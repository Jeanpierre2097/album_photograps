import passport from 'passport';
const LocalStrategy = require('passport-local').Strategy;
const googleStrategy = require('passport-google-oauth20').Strategy;
const facebookStrategy = require('passport-facebook').Strategy;
import { User } from '../models/User.entity';
import { getRepository } from 'typeorm';
import bcrypt from 'bcrypt';

const verifyCallback = async (email, password, done) => {
    try {
      const user = await getRepository(User).findOne({ email: email });
      if(!user) {
        return done(null, false, { message: 'Not user found'})
      }
      // verify the password
      if(!user.password) {
        return done(null, false, { message: 'You signup using google, register your email or login with google'})
      }
      const isValid = bcrypt.compareSync(password, user.password);
  
      if(isValid) return done(null, user) // enviamos el usuario
      else        return done(null, false, { message: "Incorrect password"})
  
    } catch (err) {
      done("Hubo un error grave", false, { message: 'Not user found'})
      console.log(err)
    }
  }
  
  const strategy = new LocalStrategy({
    usernameField: 'email', // por defecto passport usa username como el campo a buscar 
    passwordField: 'password'
  }, verifyCallback);
  
  passport.use(strategy);

// Serilize y deserialize nos ayudan a mantener la data que enviamos 
// con la cookie al minimo

//serializeUser pone al usuario dentro de la session enviando unicamente su id
passport.serializeUser((user: User, cb) => {
  cb(null, user.id);
});

// deserializeUser toma el id que enviamos con serilize y busca al nuestro user en la DB
// y despues lo coloca en req.user
passport.deserializeUser((id, cb) => {
  getRepository(User).findOne( id )
  .then(user => cb(null, user)) // el objeto user se adjunta en el request (req.user)
  .catch(error => cb(error, null));
});


// Configuracion de estrategia de Google
// Google 👇

const googleVerifyCallback = async (_, __, profile, done) => {
  console.log("profile:", profile);
  const user = await getRepository(User).findOne({ googleId: profile.id }).catch(err =>
    done(err)
  );
  if (user) {
    await getRepository(User).save(user);
    done(null, user);
  } else {
    
    const newUser = getRepository(User).create({
      googleId: profile.id,
      name: profile.displayName,
      email: profile._json.email,
    });
    await getRepository(User).save(newUser);
    console.log('New user: ' , newUser)
    done(null, newUser);
  }
}


const gStrategy =   new googleStrategy({
    clientID: process.env.GOOGLE_ID,
    clientSecret: process.env.GOOGLE_SECRET,
    callbackURL: "/auth/google/callback"
  }, googleVerifyCallback)

passport.use(gStrategy);


const facebookVerifyCallback = async (_, __, profile, done) => {
    const user = await getRepository(User).findOne({ facebookId: profile.id }).catch(err =>
      done(err)
    );
    if (user) {
      await getRepository(User).save(user);
      done(null, user);
    } else {
      
      const newUser = getRepository(User).create({
        facebookId: profile.id,
        name: profile.displayName,
        email: profile._json.email,
      });
      await getRepository(User).save(newUser);
      done(null, newUser);
    }
  }
  const fbStrategy =   new facebookStrategy({
    clientID: process.env.FACEBOOK_ID,
    clientSecret: process.env.FACEBOOK_SECRET,
    callbackURL: "/auth/facebook/callback",
    profileFields: ['id', 'displayName', 'photos', 'email']
  }, facebookVerifyCallback)

passport.use(fbStrategy);  

export default passport;
