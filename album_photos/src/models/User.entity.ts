import { Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn, CreateDateColumn} from "typeorm";
import { Place } from './places'

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: string;
    @Column()
    name: string;
    @Column()
    email: string;
    @Column({ nullable: true})
    password: string;
    
    @Column({default:"https://www.dts.edu/wp-content/uploads/sites/6/2018/04/Blank-Profile-Picture.jpg"})
    photoUrl: string;

    @Column({default: false})
    active: boolean;
  
    @CreateDateColumn()
    timestamp: Date;
    @Column({ nullable: true})
    googleId: string;
    @Column({ nullable: true})
    facebookId: string;

    @OneToMany(type => Place, place => place.user)
    @JoinColumn({name: 'place_id'}) // es opcional en relaciones MtO (pero ayuda a renombrar fields)
    place: Place[]
}