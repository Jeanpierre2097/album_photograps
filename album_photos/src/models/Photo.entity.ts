import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Photo {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ default: "Picture Post"})
  title: string;
  @Column({ default: "A picture"})
  description: string;
  @Column()
  imgName: string;
  @Column()
  imgPath: string;
  @Column("simple-json")
  location: { address: string, coordinates: number[] };
}