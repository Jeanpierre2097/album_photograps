import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn} from "typeorm";
import { User } from './User.entity'

@Entity()
export class Place {
    @PrimaryGeneratedColumn()
    id: string;
    @Column()
    name: string;
   
    @Column("simple-json")
    location: { address: string, coordinates: number[] };

    @ManyToOne(type => User, user => user.place,{ onDelete: 'CASCADE' })
    @JoinColumn({ name: 'user_id' }) // se especifica obligatoriamente en relaciones OtO y solo se define en la FK
    user: User;

    @Column('timestamp', {
        name: 'date',
        default: (): string => 'LOCALTIMESTAMP'
      })
    create_at: Date;
}