"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isLoggedIn = void 0;
exports.isLoggedIn = function (req, res, next) {
    req.isAuthenticated() ? next() : res.redirect('/login');
};
