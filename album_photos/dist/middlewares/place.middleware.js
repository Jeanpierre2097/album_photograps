"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkId = exports.isLoggedIn = void 0;
exports.isLoggedIn = function (req, res, next) {
    if (req.session.passport) {
        next();
    }
    else {
        res.redirect('/login');
    }
};
exports.checkId = function (req, res, next) {
    if (req.session.passport) {
        req.params.id === req.session.passort.user ? next() : res.redirect("/place/" + req.params.id);
    }
    else {
        res.redirect('/login');
    }
};
