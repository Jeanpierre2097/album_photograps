"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isLoggedIn = void 0;
exports.isLoggedIn = function (req, res, next) {
    console.log("isLoggedIn middleware");
    req.isAuthenticated() ? next() : res.redirect('/login');
};
// export const isActive = (req: any, res: Response, next: NextFunction) => {
//   console.log("isActive middleware")
//   req.user.active ? next() : res.send('Confirm your account');
// }
// export const authGlobal = (req: any, res: Response, next: NextFunction) => {
//   if(req.isAuthenticated()){
//     req.app.locals.logged = true;
//   } else {
//     req.app.locals.logged = false;
//   }
//   next()
// }
