"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var auth_middlewares_1 = require("../middlewares/auth.middlewares");
var cloudinary_config_1 = __importDefault(require("../config/cloudinary.config"));
var router = express_1.Router();
var index_controllers_1 = require("../controllers/index.controllers");
router.get('/', index_controllers_1.getIndex);
router.get('/feeds', index_controllers_1.getAllPlaces);
router.get('/photo/add', auth_middlewares_1.isLoggedIn, index_controllers_1.getPhotoView);
router.post('/photo/add', auth_middlewares_1.isLoggedIn, cloudinary_config_1.default.single('imgPath'), index_controllers_1.postCreatePhto);
exports.default = router;
