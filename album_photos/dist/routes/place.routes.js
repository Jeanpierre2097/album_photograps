"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var auth_middlewares_1 = require("../middlewares/auth.middlewares");
var place_controllers_1 = require("../controllers/place.controllers");
var router = express_1.Router();
router.get('/place/create', auth_middlewares_1.isLoggedIn, place_controllers_1.getPlaceCreate);
router.post('/place/create', place_controllers_1.postPlaceCreate);
router.get('/place/:id', place_controllers_1.getPlaceDetail); //este no requiere inicio de sesion
router.get("/place/:id/delete", auth_middlewares_1.isLoggedIn, place_controllers_1.PlaceDelete);
router.get("/place/:id/edit", auth_middlewares_1.isLoggedIn, place_controllers_1.getPlaceUpdate);
router.post("/place/:id/edit", auth_middlewares_1.isLoggedIn, place_controllers_1.postPlaceUpdate);
exports.default = router;
