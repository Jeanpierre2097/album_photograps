"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var auth_middlewares_1 = require("../middlewares/auth.middlewares");
var passport_config_1 = __importDefault(require("../config/passport.config"));
var auth_controllers_1 = require("../controllers/auth.controllers");
var cloudinary_config_1 = __importDefault(require("../config/cloudinary.config"));
var router = express_1.Router();
router.get('/signup', auth_controllers_1.getSignup);
router.post('/signup', auth_controllers_1.postSignup);
router.get('/login', auth_controllers_1.getLogin);
router.post('/login', auth_controllers_1.postLogin);
router.get('/logout', auth_controllers_1.logout);
router.get('/private-page', auth_middlewares_1.isLoggedIn, auth_controllers_1.privatePageGet, auth_controllers_1.profileGet);
router.post('/private-page', auth_middlewares_1.isLoggedIn, cloudinary_config_1.default.single('photoUrl'), auth_controllers_1.profilePost);
// Google auth Routes
router.get("/auth/google", auth_controllers_1.getGoogle);
router.get("/auth/google/callback", passport_config_1.default.authenticate("google", {
    successRedirect: "/private-page",
    failureRedirect: "/login"
}));
router.get('/auth/facebook', auth_controllers_1.getFacebook);
router.get('/auth/facebook/callback', auth_controllers_1.getFacebookCB);
exports.default = router;
