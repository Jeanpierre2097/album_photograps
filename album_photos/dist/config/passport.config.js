"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var passport_1 = __importDefault(require("passport"));
var LocalStrategy = require('passport-local').Strategy;
var googleStrategy = require('passport-google-oauth20').Strategy;
var facebookStrategy = require('passport-facebook').Strategy;
var User_entity_1 = require("../models/User.entity");
var typeorm_1 = require("typeorm");
var bcrypt_1 = __importDefault(require("bcrypt"));
var verifyCallback = function (email, password, done) { return __awaiter(void 0, void 0, void 0, function () {
    var user, isValid, err_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne({ email: email })];
            case 1:
                user = _a.sent();
                if (!user) {
                    return [2 /*return*/, done(null, false, { message: 'Not user found' })];
                }
                // verify the password
                if (!user.password) {
                    return [2 /*return*/, done(null, false, { message: 'You signup using google, register your email or login with google' })];
                }
                isValid = bcrypt_1.default.compareSync(password, user.password);
                if (isValid)
                    return [2 /*return*/, done(null, user)]; // enviamos el usuario
                else
                    return [2 /*return*/, done(null, false, { message: "Incorrect password" })];
                return [3 /*break*/, 3];
            case 2:
                err_1 = _a.sent();
                done("Hubo un error grave", false, { message: 'Not user found' });
                console.log(err_1);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
var strategy = new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, verifyCallback);
passport_1.default.use(strategy);
// Serilize y deserialize nos ayudan a mantener la data que enviamos 
// con la cookie al minimo
//serializeUser pone al usuario dentro de la session enviando unicamente su id
passport_1.default.serializeUser(function (user, cb) {
    cb(null, user.id);
});
// deserializeUser toma el id que enviamos con serilize y busca al nuestro user en la DB
// y despues lo coloca en req.user
passport_1.default.deserializeUser(function (id, cb) {
    typeorm_1.getRepository(User_entity_1.User).findOne(id)
        .then(function (user) { return cb(null, user); }) // el objeto user se adjunta en el request (req.user)
        .catch(function (error) { return cb(error, null); });
});
// Configuracion de estrategia de Google
// Google 👇
var googleVerifyCallback = function (_, __, profile, done) { return __awaiter(void 0, void 0, void 0, function () {
    var user, newUser;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log("profile:", profile);
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne({ googleId: profile.id }).catch(function (err) {
                        return done(err);
                    })];
            case 1:
                user = _a.sent();
                if (!user) return [3 /*break*/, 3];
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).save(user)];
            case 2:
                _a.sent();
                done(null, user);
                return [3 /*break*/, 5];
            case 3:
                newUser = typeorm_1.getRepository(User_entity_1.User).create({
                    googleId: profile.id,
                    name: profile.displayName,
                    email: profile._json.email,
                });
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).save(newUser)];
            case 4:
                _a.sent();
                console.log('New user: ', newUser);
                done(null, newUser);
                _a.label = 5;
            case 5: return [2 /*return*/];
        }
    });
}); };
var gStrategy = new googleStrategy({
    clientID: process.env.GOOGLE_ID,
    clientSecret: process.env.GOOGLE_SECRET,
    callbackURL: "/auth/google/callback"
}, googleVerifyCallback);
passport_1.default.use(gStrategy);
var facebookVerifyCallback = function (_, __, profile, done) { return __awaiter(void 0, void 0, void 0, function () {
    var user, newUser;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne({ facebookId: profile.id }).catch(function (err) {
                    return done(err);
                })];
            case 1:
                user = _a.sent();
                if (!user) return [3 /*break*/, 3];
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).save(user)];
            case 2:
                _a.sent();
                done(null, user);
                return [3 /*break*/, 5];
            case 3:
                newUser = typeorm_1.getRepository(User_entity_1.User).create({
                    facebookId: profile.id,
                    name: profile.displayName,
                    email: profile._json.email,
                });
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).save(newUser)];
            case 4:
                _a.sent();
                done(null, newUser);
                _a.label = 5;
            case 5: return [2 /*return*/];
        }
    });
}); };
var fbStrategy = new facebookStrategy({
    clientID: process.env.FACEBOOK_ID,
    clientSecret: process.env.FACEBOOK_SECRET,
    callbackURL: "/auth/facebook/callback",
    profileFields: ['id', 'displayName', 'photos', 'email']
}, facebookVerifyCallback);
passport_1.default.use(fbStrategy);
exports.default = passport_1.default;
