"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var cloudinary = require("cloudinary").v2;
var multer_storage_cloudinary_1 = require("multer-storage-cloudinary");
var multer_1 = __importDefault(require("multer"));
cloudinary.config({
    cloud_name: process.env.CLOUDINARY_NAME,
    api_key: process.env.CLOUDINARY_KEY,
    api_secret: process.env.CLOUDINARY_SECRET
});
var storage = new multer_storage_cloudinary_1.CloudinaryStorage({
    cloudinary: cloudinary,
    params: {
        folder: 'proyecto',
        allowed_formats: ['mp4', 'ogv', 'jpg', 'png', 'pdf', 'gif']
        //format: async (req, file) => 'png', // supports promises as well
        //public_id: (req, file) => 'computed-filename-using-request',
    },
});
var uploadCloud = multer_1.default({ storage: storage });
exports.default = uploadCloud;
