"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Place = void 0;
var typeorm_1 = require("typeorm");
var User_entity_1 = require("./User.entity");
var Place = /** @class */ (function () {
    function Place() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", String)
    ], Place.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Place.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column("simple-json"),
        __metadata("design:type", Object)
    ], Place.prototype, "location", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return User_entity_1.User; }, function (user) { return user.place; }, { onDelete: 'CASCADE' }),
        typeorm_1.JoinColumn({ name: 'user_id' }) // se especifica obligatoriamente en relaciones OtO y solo se define en la FK
        ,
        __metadata("design:type", User_entity_1.User)
    ], Place.prototype, "user", void 0);
    __decorate([
        typeorm_1.Column('timestamp', {
            name: 'date',
            default: function () { return 'LOCALTIMESTAMP'; }
        }),
        __metadata("design:type", Date)
    ], Place.prototype, "create_at", void 0);
    Place = __decorate([
        typeorm_1.Entity()
    ], Place);
    return Place;
}());
exports.Place = Place;
