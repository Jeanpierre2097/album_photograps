"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFacebookCB = exports.getFacebook = exports.getGoogleCB = exports.getGoogle = exports.privatePageGet = exports.logout = exports.postLogin = exports.profilePost = exports.profileGet = exports.getLogin = exports.postSignup = exports.getSignup = void 0;
var User_entity_1 = require("../models/User.entity");
var places_1 = require("../models/places");
var typeorm_1 = require("typeorm");
var passport_config_1 = __importDefault(require("../config/passport.config"));
var bcrypt_1 = __importDefault(require("bcrypt"));
exports.getSignup = function (req, res, next) {
    res.render('auth/signup');
};
exports.postSignup = function (req, res) {
    req.logout();
    var _a = req.body, name = _a.name, email = _a.email, password = _a.password;
    if (name === '' && email === '' && password === '') { //si el usuario o contraseña estan vacios, manda mensaje de error
        res.render("auth/signup", { message: "Por favor llene todos los campos" });
    }
    typeorm_1.getRepository(User_entity_1.User).findOne({ email: email })
        .then(function (user) {
        if (!user) {
            var salt = bcrypt_1.default.genSaltSync(10);
            var hashPassword = bcrypt_1.default.hashSync(password, salt);
            var newUser = typeorm_1.getRepository(User_entity_1.User).create({ name: name, email: email, password: hashPassword });
            typeorm_1.getRepository(User_entity_1.User).save(newUser)
                .then(function () {
                res.redirect('/');
            })
                .catch(function (err) { return console.log(err); });
        }
        else {
            res.render("auth/signup", { message: "El correo " + email + " ya ha sido registrado" });
        }
    }).
        catch(function (error) { return console.log(error); });
};
exports.getLogin = function (req, res, next) {
    var message = req.flash("error")[0];
    res.render('auth/login', { message: message });
};
//PROFILE
exports.profileGet = function (req, res, next) {
    var user = req.user;
    res.render('private-page', user);
};
exports.profilePost = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var path, id, user;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log("Que nos responde cloudinary: ", req.file);
                path = req.file.path;
                id = req.user.id;
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).update(id, { photoUrl: path })];
            case 1:
                user = _a.sent();
                req.user = user;
                res.redirect('/private-page');
                return [2 /*return*/];
        }
    });
}); };
/**
 * Se requieren dos rutas para la autenticación OAuth.
 * La primera ruta inicia una transacción OAuth y redirige al usuario al proveedor de servicios.
 * La segunda ruta es la URL a la que se redirigirá al usuario después de autenticarse con el proveedor.
 */
exports.postLogin = passport_config_1.default.authenticate("local", {
    successRedirect: "/private-page",
    failureRedirect: "/login",
    failureFlash: true
});
exports.logout = function (req, res, next) {
    req.session.destroy(function () {
        req.logOut();
        res.clearCookie('graphNodeCookie');
        res.status(200);
        res.redirect('/login');
    });
};
exports.privatePageGet = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user, userId, places, err_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                user = req.user;
                userId = user['id'];
                console.log('El usuario que quiere ver sus fotos es: ', user['email']);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, typeorm_1.getRepository(places_1.Place)
                        .createQueryBuilder("place")
                        .where("user_id = :user_id", { user_id: userId })
                        .orderBy("place.create_at", "ASC").execute()];
            case 2:
                places = _a.sent();
                console.log(places);
                res.render('private-page', { places: places, user: user });
                return [3 /*break*/, 4];
            case 3:
                err_1 = _a.sent();
                console.log(err_1);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.getGoogle = (passport_config_1.default.authenticate("google", {
    scope: [
        "https://www.googleapis.com/auth/userinfo.profile",
        "https://www.googleapis.com/auth/userinfo.email"
    ]
}));
exports.getGoogleCB = (passport_config_1.default.authenticate("google", {
    successRedirect: "/private-page",
    failureRedirect: "/login"
}));
exports.getFacebook = (passport_config_1.default.authenticate("facebook", {
    scope: ['email']
}));
exports.getFacebookCB = (passport_config_1.default.authenticate("facebook", {
    successRedirect: "/private-page",
    failureRedirect: "/login"
}));
