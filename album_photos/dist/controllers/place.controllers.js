"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlaceDelete = exports.postPlaceUpdate = exports.getPlaceUpdate = exports.getPlaceDetail = exports.postPlaceCreate = exports.getPlaceCreate = void 0;
var typeorm_1 = require("typeorm");
var places_1 = require("../models/places");
exports.getPlaceCreate = function (req, res) {
    var user = req.user;
    res.render('place/create', { user: user });
};
exports.postPlaceCreate = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, name, address, latitude, longitude, placeType, idUser, newPlace, place;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = req.body, name = _a.name, address = _a.address, latitude = _a.latitude, longitude = _a.longitude, placeType = _a.placeType, idUser = _a.idUser;
                newPlace = {
                    name: name,
                    user: idUser,
                    placeType: placeType,
                    location: {
                        address: address,
                        coordinates: [longitude, latitude]
                    }
                };
                place = typeorm_1.getRepository(places_1.Place).create(newPlace);
                return [4 /*yield*/, typeorm_1.getRepository(places_1.Place).save(place)];
            case 1:
                _b.sent();
                console.log('lugar insertado');
                res.redirect('/private-page');
                return [2 /*return*/];
        }
    });
}); };
exports.getPlaceDetail = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, place;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                return [4 /*yield*/, typeorm_1.getRepository(places_1.Place).findOne(id)];
            case 1:
                place = _a.sent();
                res.render('place/placeDetail', place);
                return [2 /*return*/];
        }
    });
}); };
exports.getPlaceUpdate = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, user, place, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                user = req.user;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, typeorm_1.getRepository(places_1.Place).findOne(id)];
            case 2:
                place = _a.sent();
                res.render("place/editPlace", { place: place, user: user });
                return [3 /*break*/, 4];
            case 3:
                error_1 = _a.sent();
                console.log(error_1);
                next();
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.postPlaceUpdate = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, _a, name, address, latitude, longitude, placeType, idUser, error_2;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                id = req.params.id;
                _a = req.body, name = _a.name, address = _a.address, latitude = _a.latitude, longitude = _a.longitude, placeType = _a.placeType, idUser = _a.idUser;
                console.log('la direccion es: ', address);
                _b.label = 1;
            case 1:
                _b.trys.push([1, 4, , 5]);
                return [4 /*yield*/, typeorm_1.getRepository(places_1.Place).findOne(id)];
            case 2:
                _b.sent();
                return [4 /*yield*/, typeorm_1.getRepository(places_1.Place).update(id, {
                        name: name,
                        user: idUser,
                        location: {
                            address: address,
                            coordinates: [longitude, latitude]
                        }
                    })];
            case 3:
                _b.sent();
                res.redirect("/private-page");
                return [3 /*break*/, 5];
            case 4:
                error_2 = _b.sent();
                console.log(error_2);
                next();
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); };
exports.PlaceDelete = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, typeorm_1.getRepository(places_1.Place).delete(id)];
            case 2:
                _a.sent();
                res.redirect('/private-page');
                return [3 /*break*/, 4];
            case 3:
                error_3 = _a.sent();
                console.log(error_3);
                next();
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
