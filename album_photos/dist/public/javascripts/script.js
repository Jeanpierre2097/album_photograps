mapboxgl.accessToken = "pk.eyJ1IjoiZWR4ejdjIiwiYSI6ImNrYjJibnoycTA2bmYzMGs3aXcyNmFjZ2IifQ.2skhXuOsH9YdLh4feNv4xg";
const kavakLerma = [-99.484685, 19.278897];

const map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/mapbox/streets-v11',
  center: kavakLerma,
  zoom: 13
});

const geocoder = new MapboxGeocoder({
  accessToken: mapboxgl.accessToken,
  mapboxgl: mapboxgl,
  countries: 'mx'
});

geocoder.on('result', (result) => {
  let [ lng, lat ] = result.result.center;
  console.log(lng, lat);
  if(lng  || lat ) {

    new mapboxgl.Marker().setLngLat([ lng, lat ]).addTo(map);
    const mapboxInput = document.querySelector('input[placeholder="Search"]').value
    const addressInput = document.querySelector('input[name="address"]');
    const latInput = document.querySelector('input[name="latitude"]');
    const lngInput = document.querySelector('input[name="longitude"]');
    latInput.value = lat;
    lngInput.value = lng;
    addressInput.value = mapboxInput;
  }
})

map.addControl(geocoder);


